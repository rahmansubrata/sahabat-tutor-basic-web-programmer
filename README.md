# Sahabat Tutor-Basic Web Programmer

## Getting started

Selamat Datang Di Pelatihan Series From Zero To Hero Programmers For The Basics of The Startup Ecosystem
Basic Arduino Simulation - Basic Web Programmer - Basic Python Image Procesing

Basic Web library Codeigniter 4-Basic Spatial Analyst with ArcGis
presented by sahabattutor.com

## About Sahabat Tutor Training Series

Pelatihan ini diperuntukan bagi orang-orang yang ingin belajar dasar-dasar menjadi programmer hebat di masa datang. (note pelatihan ini bersipat basic tidak cocok untuk yang tingkat advance atau programmer lanjutan). Pelatihan dilakukan secara online melalui aplikasi zoom meeting.

Output dari pelatihan ini yaitu diharapkan menjadi dasar yang kuat bagi pemula yang akan belajar programming sehingga dapat peserta diharapkan dapat mengembangkan dasar pengetahuannya untuk dunia programmer. Peserta pelatihan mampu untuk melakukan penyelesaian dari error sehingga mampu belajar dengan cepat dari banyak tutorial yang sudah ada di internet maupun youtube.

## Description

Pelatihan diadakan secara online pada 28 agustus 2021 sampai 18 September 2021

Pelatihan ini terdiri dari beberapa series yang bisa diikuti

1. Pelatihan simulation control otomatic basic simulation Arduino with proteus.
2. Pelatihan basic web programmer with php for backend end bootstrap for frontend , and database mysql
3. Pelatihan Basic programmer python for image processing with opencv library image for data scient
4. Pelatihan Basic Spasial Analiyst with ArcGis
5. Pelatihan basic code for fullstack developer with framework Codeigniter 4

## Installation

1. Download XAMPP pada link
   https://www.apachefriends.org/download.html

2. Install vscode
   https://code.visualstudio.com/download

3. pindahkan pada folder htdocs , jika isntall secara default maka lokasi pada C:\xampp\htdocs
4. Import file SQL

## Usage

Souce code pelatihan dasar programmer untuk dapat melakukan basic create , read, update, and delete with php.

## Contributing

thankyou for all contributed
Rahman Subrata as trainer
Tri Abdi Fauzi as trainer
Galang Yusal Farisi as Project manejer

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

https://www.w3schools.com/
https://markey.id/blog/development/pemrograman-web-adalah
https://www.dicoding.com/blog/apa-itu-database/
https://www.php.net/docs.php
https://www.youtube.com/watch?v=oWriY67kGQ0
https://www.youtube.com/watch?v=0KCk_uhvu64
https://getbootstrap.com/

## More infromation

more information
http://sahabattutor.com/pelatihan
https://play.google.com/store/apps/details?id=id.codepanda.sahabattutor.tutor&hl=en&gl=US

information
Join with sahabattutor as tutor
ipb.link/panduan-pendaftaran-pengajar-st
