<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "latihan";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    echo "Not Connected";
}

// mengambil data dari tabel dalam database
// $query = ("SELECT * FROM mahasiswa");
// $result = mysqli_query($conn, $query);
//var_dump($result);

//mengambil data dari object result (fetch)
// mysqli_fetch_row()
// mysqli_fetch_assoc()
// mysqli_fetch_array()
// mysqli_fetch_object()
// $data = mysqli_fetch_row($result);
// var_dump($data);

//looping data
// while ($data = mysqli_fetch_row($result)) {
//     var_dump($data);
// }

//
